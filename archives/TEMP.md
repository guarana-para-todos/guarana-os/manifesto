
### Diagrama de Atividade

> TODO: Descreva como funcionará etapa de transformar código intermediário em código de maquina, de acordo com o diagrama de atividade abaixo


Pilha de instruções para cada processo

```plantuml
@startuml
(*) --> [pelo menos um teste disponível] "Cria Pilha de Chamada"
    --> "Adiciona Chamada de Armazenamento do Plano"
    --> "Seleciona Notificador Global" as VR
    if "" then
        -left---> [disponível] "Adiciona Chamada de Notificação Global" as ANG
        ANG --up-> VR
    else
        -right-> [else] "Seleciona Teste" as PT
        if "" then
                --> [disponível] "Seleciona Notificador de Teste" as VRT
                if "" Then
                        -right-> [disponível] "Adiciona Chamada de Notificação de teste" as AN
                        AN ---> VRT 
                else
                        --> [else] "Adiciona Chamada de Armazenamento de Relatório"
                        --> "Adiciona Chamada de Execução de Teste"
                        --> PT
                endif
        else
                --> [else] (*)
    endif
@enduml
```

---

