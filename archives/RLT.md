# Relatório

## LOG

| Autor            | Email                 | Proposta          | Data/Hora Proposta |
| ---------------- | --------------------- | ----------------- | ------------------ |
| Jefferson Lisboa | lisboa.jeff@gmail.com | Definição inicial | 03/04/2023 13:30   |

## Discussão

Abaixo temos um modelo de resposta esperada para o relatório, descrita pelos seguintes itens:

1. Resultado do teste
2. Nome detalhado do teste realizado
3. Metadados do componente
4. Metadados da requisição da troca de mensagem entre o agente e servidor
5. Data/Hora
6. Resultado Esperado
7. Resultado Obtido

### Nota

1. Um plano pode contém 1 ou muitos testes, portanto um relatório deve conter no mínimo uma descrição de teste
2. Cada Agente pode especificar o formato do relatório próprio. A proposta acima não define uma estrutura padrão

## Exemplo Relatório

```json
[
    {
        "resultado" : "falhou",
        "teste" : "Deve recuperar informações do estado de São Paulo",
        "componente" : {
            "repositório" : "git.knin.com/projetos/ibge",
            "nome" : "IBGE-BACKEND" 
        }
        "data_hora" : "30/03/2023 00:26:00 GMT-3",
        "requisição" : {
            "verbo" : "GET",
            "url" : "ibge.dev.knin.com/estados/sao_paulo/informações",
            "headers" : {
                "content-type" : "application/json"
            }
        },
        "esperado" :  {
            "body" : {
                "id":35,
                "sigla":"SP",
                "nome":"São Paulo",
                "região": {
                    "id":3,
                    "sigla":"SE",
                    "nome":"Sudeste"
                }
            }
        },
        "obtido" :  {
            "body" : {
                "id" : 35,
                "nome" : "São Paulo",
            }
        }
    }
]
```