# Plano de Testes

## LOG

| Autor            | Email                 | Proposta                             | Data/Hora Proposta |
| ---------------- | --------------------- | ------------------------------------ | ------------------ |
| Jefferson Lisboa | lisboa.jeff@gmail.com | Definição inicial                    | 03/04/2023 13:40   |
| -                | -                     | Definindo as etapas de interpretação | 05/04/2023 15:13   |

## Definição

Plano de testes é um módulo representando por uma linguagem específica de domínio (DSL) chamada plan (Linguagem de Programação para Planejamento de testes)

| Palavra Chave | Descrição                                                                                                     |
| ------------- | ------------------------------------------------------------------------------------------------------------- |
| component     | Expressão comando identificar o módulo de planejamento de testes                                              |
| notifier      | Expressão comando com proposito de notificar fim de execução de um teste                                      |
| test          | Expressão comando com proposito de executar um teste                                                          |
| driver        | Expressão comando identifica o driver e atributos responsável pela comunicação entre o sistema e dispositivos |


## Discussão

1. Declara um conjunto de testes executados por operadores. Abaixo temos os tipos de testes disponíveis:

| Tipo     | Descrição                                                                                              |
| -------- | ------------------------------------------------------------------------------------------------------ |
| required | Quando é dependente de um plano de testes cujo componente foi alterado, é obrigatório que seja testado |
| optional | Quando é dependente de um plano de testes cujo componente foi alterado, sua execução não é exigida     |

## Exemplo

```clojure
(component "X"
    (test required
        (driver "newman"
            :collection  (file "test.json")
            :environment (file "environment.json")
        )
    )
)
```
---

1. Declara um conjunto de notificadores cujo proposito é encaminhar mensagem de fim de execução para os responsáveis pelo componente

## Exemplo

```clojure
(component "IBGE"
    
    (notifier [success] 

        (driver "Email"
            :message "Plano de testes do componente IBGE foi realizado com sucesso"
            :email "ibge@empresa.com"
        )
        
    )
    
    (test optional
        
        (driver "newman"    
            :collection  (file "tests/newman/collection.json")
            :environment (file "tests/newman/environment.json")
        )

        (notifier [failed] 

            (driver "Email"
                :message (file "tests/newman/email-falha.html")
                :email "ibge@empresa.com"
            )

        )
    )
)
```

Quando todos os testes são finalizados com sucesso, uma notificação será encaminhada para os responsáveis do componente IBGE. Por outro lado, caso o teste opcional seja finalizado com falha, uma notificação será encaminhada para os responsáveis 

---

1. O sistema operacional para execução do plano de teste é divido em três etapas. Temos a parte do Código Intermediário (Front End) e Código Maquina (Backend) e por fim a Maquina Virtual

```mermaid
flowchart LR

    id1{{Plano Testes}} --> F[Compilador Front End]  
    id2{{Arquivos}} --> F
    F --Código Intermediário-->  B[Compilador Back End]
    B --Código de Maquina--> M[Maquina Virtual]

```

## Compilador Front End

O compilador Front End *codificará* todos os **arquivos** (*UTF-8*) importados em **Base 64**

## Exemplo Plano de Testes

```clojure
(component "X"

    (notifier [success] 

        (driver "Email"
            :message "Plano de testes do componente IBGE foi realizado com sucesso"
            :email "ibge@empresa.com"
        )
        
    )

    (test required

        (driver "newman"
            :collection (file "test.json")
            :proxy "http://proxy.dev.knin-project.online:888"
        )

        (notifier [failed] 
            (driver "email"
                :message (file "falha.html")
                :email "ibge@empresa.com"
            )
        )

    )

)
```

## Arquivo test.json

```json
{
    "url" : "ibge.dev.knin.com/estados/sao_paulo/informações",
    "verbo" : "GET",
    "headers" : {
        "content-type" : "application/json"
    },
    "esperado" : {
        "body" : {
            "id":35,
            "sigla":"SP",
            "nome":"São Paulo",
            "região": {
                "id":3,
                "sigla":"SE",
                "nome":"Sudeste"
            }
        }  
    }
}
```

## Arquivo falha.html

```html
<!DOCTYPE html>
<html>
    <body>
        <h1>My First Heading</h1>
        <p>My first paragraph.</p>
    </body>
</html> 
```
## Código Intermediário

```clojure
(componente "X"

    (test required
        (driver "newman"
            :collection "ewogICAgInVybCIgOiAiaWJnZS5kZXYua25pbi5jb20vZXN0YWRvcy9zYW9fcGF1bG8vaW5mb3JtYWNvZXMiLAogICAgInZlcmJvIiA6ICJHRVQiLAogICAgImhlYWRlcnMiIDogewogICAgICAgICJjb250ZW50LXR5cGUiIDogImFwcGxpY2F0aW9uL2pzb24iCiAgICB9LAogICAgImVzcGVyYWRvIiA6IHsKICAgICAgICAiYm9keSIgOiB7CiAgICAgICAgICAgICJpZCI6MzUsCiAgICAgICAgICAgICJzaWdsYSI6IlNQIiwKICAgICAgICAgICAgIm5vbWUiOiJTw6NvIFBhdWxvIiwKICAgICAgICAgICAgInJlZ2lhbyI6IHsKICAgICAgICAgICAgICAgICJpZCI6MywKICAgICAgICAgICAgICAgICJzaWdsYSI6IlNFIiwKICAgICAgICAgICAgICAgICJub21lIjoiU3VkZXN0ZSIKICAgICAgICAgICAgfQogICAgICAgIH0gIAogICAgfQp9"
            :proxy "http://proxy.dev.knin-project.online:888"
        )
        (notifier [failed] 

            (driver "email"
                :message "PCFET0NUWVBFIGh0bWw+CjxodG1sPgogICAgPGJvZHk+CiAgICAgICAgPGgxPk15IEZpcnN0IEhlYWRpbmc8L2gxPgogICAgICAgIDxwPk15IGZpcnN0IHBhcmFncmFwaC48L3A+CiAgICA8L2JvZHk+CjwvaHRtbD4g"
                :address "ibge@empresa.com"
            )

        )
    )

    (notifier [success] 

        (driver "Email"
            :message "Plano de testes do componente IBGE foi realizado com sucesso"
            :address "ibge2@empresa.com"
        )

    )

)
```

## Compilador Backend

O compilador Back end transformará o código intermediário em [código de maquina](../code/process.s)