# Componentes

## LOG

| Autor            | Email                 | Proposta                                 | Data/Hora Proposta |
| ---------------- | --------------------- | ---------------------------------------- | ------------------ |
| Jefferson Lisboa | lisboa.jeff@gmail.com | Definição inicial                        | 30/04/2023 19:24   |
| -                | -                     | Descrição das atividades dos componentes | 03/05/2023 20:00   |
| -                | -                     | Descrição do protocolo de comunicação    | 13/05/2023 00:12   |
| -                | -                     | Definindo distribuição                   | 18/05/2023 01:21   |

## Objetivo

Definir componentes exigidos para resolução do problema de planejamento e execução de plano de testes

## Discussão

Em modo geral os principais componentes do sistema serão desenvolvidos em forma de módulos agnósticos a qualquer detalhe de tecnologia de comunicação, base de dados, framework e etc.

```mermaid
flowchart TD

id01[Kernel]
id02[Servidor de Testes]
id03[Servidor de Notificação]
id04[Gerenciador de Planos]

```

Por serem componentes canônicos do projeto **guarana OS**, devem ser livre de qualquer contexto concreto do ambiente que serão implantados. A decisão leva em consideração que é da escolha da equipe de engenharia utilizar o sistema, seja em alguma plataforma de nuvem, servidor próprio ou na própria maquina de desenvolvimento.

### Atividades por módulos

| Kernel                                 | Servidor de Testes                        | Servidor de Notificação | Gerenciador de Planos                         |
| -------------------------------------- | ----------------------------------------- | ----------------------- | --------------------------------------------- |
| Traduzir Código de Maquina em Processo | Registrar Servidor                        | Registrar Servidor      | Traduzir plano de testes em código de máquina |
| Escalonar Processo                     | Executar Teste                            | Realizar Notificação    | Armazenar plano de testes                     |
| Interromper Processo Bloqueado         | Gerenciar Drivers                         | Gerenciar Drivers       | Recuperar plano de testes de dependentes      |
| Interpretar Código de Máquina          | Armazenar Relatório                       |                         |                                               |
| Receber Registro Servidor              | Notificar Resultado de execução ao Kernel |                         |                                               |
| Receber Mensagem de Interrupção        |                                           |                         |                                               |

Caso o desenvolvedor/engenheiro/equipe não encontre disponível uma distribuição para seu ambiente no repositório de distribuições, obrigatoriamente precisará construir sua própria distribuição seguindo o **guia** de desenvolvimento

---

## Diagrama de Componentes

Abaixo temos disponível um digrama de componente apresentando uma visão macro do projeto guarana OS dentro de uma distribuição hipotética

```mermaid
flowchart TD

     subgraph Modo Driver
          id10[<< Notificador >>\nDriver Email]
          id11[<< Testador >>\nDriver Newman]
          id13[<< Testador >>\nDriver RabbitMQ]
     end

     subgraph Aplicação
          id12(Shell)
     end

     subgraph Modo Servidor
          id1(Servidor Notificação)
          id2(Servidor Teste)
          id4(Gerenciador de Planos)
          id5(Servidor Informações)
     end

     subgraph Modo Kernel
          id6(Gerenciador de Processos)
          id8(Gerenciador de Memória)
          id9(Gerenciador de Servidores)
     end

     id1 <--> id9
     id2 <--> id9
     id4 <--> id9
     id5 <--> id9
     
     id6 --> id8

     id9 <--> id6

     id10 <--> id1
     id11 <--> id2
     id13 <--> id2
     
     id12 --> id4

```

## Gerenciador de Servidores

A interface de comunicação do kernel corresponde ao gerenciador de servidores de acordo com as seguintes chamadas externa habilitadas: 

| Chamada Externa           | DATA_1      | DATA_2     | DATA_3   | DATA_4   | Objetivo                             |
| ------------------------- | ----------- | ---------- | -------- | -------- | ------------------------------------ |
| [reg](../code/register.s) | server_name | type       | channel  | driver_0 | Registrar Servidor                   |
| [up](../code/update.s)    | key         | driver_0   | driver_1 | driver_2 | Atualizar Drivers                    |
| [send](../code/send.s)    | key         | pd         | valor_1  | valor_2  | Enviar Mensagem a Processo Suspenso  |
| [ping](../code/ping.s)    | key         | -          | -        | -        | Informar disponibilidade do servidor |
| [new](../code/process.s)  | key         | code (B64) | -        | -        | Iniciar novo processo                |

### Chamada Externa - REG (Registro)

A chamada externa 'REG' tem como objetivo registrar um servidor ao sistema

| Atributo    | Descrição                                                |
| ----------- | -------------------------------------------------------- |
| server_name | Nome de identificação do servidor                        |
| type        | Deve identificar se o servidor é do tipo : exec ou write |
| channel     | Canal de comunicação para encaminhar eventos             |
| driver      | Identificação dos drivers disponíveis  (1 ou vários)     |

#### Exemplo de REG

```csv
reg test  exec test-013234432-channel   newman    curl
```

Após o gerenciador de servidores receber uma chamada externa para registro, ele encaminhará para o canal a seguinte mensagem

| Atributo    | Descrição                                                    |
| ----------- | ------------------------------------------------------------ |
| -           | chamada de callback para o servidor requisitante             |
| server_name | Nome de identificação do servidor                            |
| key         | Chave de autorização de acesso as chamadas externa           |
| time        | Tempo limite  para o servidor encaminhar um ping de resposta |

```
callback server_name     32484923443783434435  60
```
---

### Chamada Externa - UP (Atualização)

A chamada externa 'UP' tem como objetivo atualizar a tabela dos drivers disponíveis, seja para remoção ou para adição

| Atributo | Descrição                                            |
| -------- | ---------------------------------------------------- |
| key      | Chave de autorização de acesso as chamadas externa   |
| driver   | Identificação dos drivers disponíveis  (1 ou vários) |

#### Exemplo de UP

```csv
up test  exec test-013234432-channel   newman    curl
```

---

### Chamada Externa - SEND (Notificação de Interrupção)

Quando um processo precisa trocar mensagem com um servidor, devemos realizar uma chamada de sistema **call**

```assembly
mov ax, v1_driver
add ax, v2_collection
add ax, v3_proxy
call _exec
```

No exemplo acima, iremos enviar uma mensagem de execução para o servidor que responde pelo driver identificado no registro de memória **v1_driver** 

| Atributo             | Descrição                                                                |
| -------------------- | ------------------------------------------------------------------------ |
| tipo                 | Tipo de execução a ser feita                                             |
| processor descriptor | Endereço virtual para acessar memória e interrupção do processo suspenso |
| value                | cadeia de string representando a mensagem de notificação (1 ou vários)   |

Exemplo da troca de mensagem com servidor

```
exec 30202183247111 driver="..." collection="..."  proxy="..."
```

----

Para um servidor trocar mensagem com o **sistema operacional**, será necessário realizar uma chamada externa 'SEND' com proposito de efetuar uma interrupção ao processo bloqueado. Lembrando que servidores registrado com tipo diferente de **exec**, serão ignorados

| Atributo             | Descrição                                                                |
| -------------------- | ------------------------------------------------------------------------ |
| key                  | Chave de autorização de acesso as chamadas externa                       |
| processor descriptor | Endereço virtual para acessar memória e interrupção do processo suspenso |
| value                | cadeia de string representando a mensagem de notificação (1 ou vários)   |

#### Exemplo de SEND

```csv
send 32484923443783434435  30202183247111   0 1   oi
```

---

### Chamada Externa - PING

A chamada externa 'PING' tem como objetivo informar a disponibilidade do servidor. Caso o servidor não esteja funcionando, a longo do tempo, a tendência é que o sistema o retire da tabela de servidores disponíveis

| Atributo | Descrição                                          |
| -------- | -------------------------------------------------- |
| key      | Chave de autorização de acesso as chamadas externa |

#### Exemplo de PING

```csv
ping 32484923443783434435
```

Caso o tempo limite ultrapasse o valor informado, um processo irá remover seu registro do sistema operacional

---

### Chamada Externa - NEW (Iniciar Novo Processo)

A chamada externa 'NEW' tem como objetivo iniciar um processo a partir da interpretação do código de maquina

| Atributo | Descrição                                          |
| -------- | -------------------------------------------------- |
| key      | Chave de autorização de acesso as chamadas externa |
| code     | Código de maquina em base 64                       |

#### Exemplo de NEW

```csv
new 32484923443783434435 "código de maquina em base 64"
```

