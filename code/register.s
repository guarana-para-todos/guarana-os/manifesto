; REGISTRAR SERVIDOR

section	 .data

    v0_server   db "test"
    v1_type     db "exec"
    v2_channel  db "test-01033--23234234"

section .text

main:

    mov cx, v0_server
    add cx, v1_type
    add cx, v2_channel

    call _register ; register(cx,esp)

    ret
