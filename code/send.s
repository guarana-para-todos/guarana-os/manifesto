; QUANDO SISTEMA RECEBE MENSAGEM RESPOSTA DE UM SERVIDOR, PRECISAMOS NOTIFICAR O PROCESSO SUSPENSO
;
; 32210321312 (descritor de processo que representa stack e procedimento de interrupção do processo)
;    2398     (Valor)
;

section .text

main:
    pop ax, esp     ; retira valor da pilha e copia para registrador (Descritor de Processo)
A:  pop bx, esp     ; retira valor da pilha e copia para registrador (Valor da Pilha)
    push ax, bx     ; adiciona no endereço registrador (ax [stack]) um valor (bx). 
                    ; Considerando que ax seja um endereço de pilha
    cmp  0x0, esx   ; compara tamanho da pilha com 0
    jne  A          ; caso pilha.tamanho != 0 , pula para A
    call _interrupt ; realiza interrupção do processo para retirar da suspensão [entrada: ax]
    ret             ; encerra/finaliza processo
