---
title: "Construindo um sistema operacional para planejar e executar testes para um ambiente de sistemas distribuídos"
author:
    - Jefferson Lisboa de Souza <lisboa.jeff@gmail.com>
date: April 25, 2023
---

![logo](images/logo.png)

## Objetivo

Planejar e executar testes para um ambiente de sistemas distribuídos com perspectiva de garantir feedback após implantação de novos componentes

## Motivação

Gerenciar um ambiente de sistemas distribuídos não é uma tarefa fácil. Temos muitas inter-relações e quando algum desses componentes sofrem algum tipo de efeito colateral, podemos gerar uma falha sistêmica difícil de identificar. Encontrar o agente torna-se uma tarefa árdua para uma equipe de engenharia.

O **Guarana OS** tem como objetivo gerenciar e oferecer um portal de monitoramento dos planos de testes para ambiente de sistemas distribuídos, visando o feedback das falhas sistêmicas após a realização de novas implantações de componentes.

---

## Recursos

1. [Plano de Teste](archives/PT.md)
2. [Relatório](archives/RLT.md)

## Componentes

1. [Servidor Testes](archives/COMP.md)
2. [Servidor Notificação](archives/COMP.md)
3. [Gerenciador de Planos](archives/COMP.md)

## Atores

| Identificação | Descrição     |
| ------------- | ------------- |
| DEV           | Desenvolvedor |
| RLT           | Relator       |

## Casos de Uso

| Identificação          | Descrição            | Objetivo                                                                    |
| ---------------------- | -------------------- | --------------------------------------------------------------------------- |
| [A01](archives/A01.md) | Executar Plano       | Executar plano de testes e propiciar relatório para investigação            |
| [A02](archives/A02.md) | Investigar Relatório | Investigar relatório propiciado a partir do plano de testes                 |
| [A03](archives/A03.md) | Notificar Execução   | Notificar fim de execução do plano de testes para responsável do componente |
| [A04](archives/A04.md) | Integrar Componente  | Integrar componente num conjunto de ligações do sistema                     |
| [A05](archives/A05.md) | Excluir Componente   | Excluir componente do conjunto de ligações do sistema                       |
| [A06](archives/A05.md) | Notificar Exclusão   | Notificar exclusão de um componente ao responsável                          |
---

## Diagrama de Casos de Uso

```plantuml

actor Desenvolvedor as DEV
actor Relator << System >> as RLT 

usecase "Executar Plano" as A01

usecase "Investigar Relatório" as A02

usecase "Notificar Execução" as A03

usecase "Notificar Exclusão" as A06

usecase "Integrar Componente" AS A04

usecase "Excluir Componente" as A05

DEV --> A01
DEV --> A02
DEV --> A04
DEV --> A05

A05 ..|> A06 : <<include>>

RLT --> A03


```

---

Abaixo temos a representação da maquina de estado de acordo com os casos de uso

```plantuml
@startuml
state Desenvolvimento {

[*] --> Planejado : . [plano definido]
Planejado --> Planejado : atualizarPlano
Planejado --> Execução : executar teste
Execução -down-> Execução : executarTesteDeOutroPlano
Execução --> Notificação : . [relatório propiciado]
Notificação -right-> Investigação : . [responsável notificado]
Investigação : Entry / investigarRelatório()
Investigação : Do / efetuarPlanoAção()
Investigação : Exit / concluirInvestigação()
Investigação -left-> Planejado : . [investigação concluída]
}
Desenvolvimento --> [*] : . [componente excluído]
@enduml

```